package helloApp;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class TestAppHello {
	
    App service;

	@Before
	public void setUp() throws Exception {
		service = Mockito.mock(App.class);
	}

	@After
	public void tearDown() throws Exception {
		service = null;
	}

	@Test
	public void testHello() {
		when(service.Hello()).thenReturn("Mathieu");
		assertEquals("Mathieu", service.Hello());
	}

}
